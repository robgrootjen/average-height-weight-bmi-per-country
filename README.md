The dataset shows the average height, weight and BMI from male and female per country. The dataset includes 6 graphs to have a beter overview.

## Data
The data is sourced from https://www.worlddata.info/average-bodyheight.php

The height is in centimeters and the weight in kilograms

--------------------------------------------------------------------------------------------------------------------------------

## Preparation

Requires:
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

Instructions:
* Copy process.py script in the "Process" folder.
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* 2 CSV´s file will be saved in document where your terminal is at the moment.

----------------------------------------------------------------------------------------------------------------------------------------

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 