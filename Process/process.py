import requests
import time
import csv
from bs4 import BeautifulSoup
from selenium import webdriver

#Main function
def getContent(link):
    #Open Browser
    browser = webdriver.Chrome()

    #Go to link
    browser.get(link)

    #Pause
    time.sleep(3)

    #Source
    html = browser.page_source

    #Soup activate
    soup = BeautifulSoup(html,'lxml')

    #Find table
    table = soup.find_all('table')
    table2 = table[1]
    
    #Save in csv
    with open('averageheight.csv','w',newline='') as f:
        writer = csv.writer(f)
        writer.writerow(('Country','Average Height Male','Average Weight Male','Average BMI Male','Average Height Female','Average Weight Female','Average BMI Female'))
        for tr in table2('tr')[1:]:
            row = [(t.get_text(strip=True)).split(" ")[0] for t in tr(['td','th'])]
            cell1 = row[0] 
            cell3 = row[2]
            cell4 = row[3]
            cell5 = row[4]
            cell7 = row[6]
            cell8 = row[7]
            cell9 = row[8]
            row = [cell1,cell3,cell4,cell5,cell7,cell8,cell9]   
            writer.writerow(row)

    #Close browser
    browser.close()
    browser.quit()

#Links
getContent('https://www.worlddata.info/average-bodyheight.php')